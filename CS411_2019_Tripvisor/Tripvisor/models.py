from django.db import models
from django.urls import reverse
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo, DateProperty
# from django.contrib.auth.models import User


class Location(models.Model):
    location_id = models.AutoField(primary_key=True)
    city = models.CharField(max_length=45)
    state = models.CharField(max_length=45)
    area = models.CharField(max_length=45)

    def __str__(self):
        return '%s, %s - %s' % (self.city, self.state, self.area)

    def get_absolute_url(self):
        return reverse('Tripvisor_location_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_update_url(self):
        return reverse('Tripvisor_location_update_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    def get_delete_url(self):
        return reverse('Tripvisor_location_delete_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['city', 'state', 'area']
        unique_together = (('city', 'state'),)


class Attraction(models.Model):
    attraction_id = models.AutoField(primary_key=True)
    location = models.ForeignKey(Location, related_name='tours', on_delete=models.PROTECT)  # tours->locations
    attraction_name = models.CharField(max_length=45, unique=True)
    type = models.CharField(max_length=45)
    price = models.IntegerField(unique=False)
    address = models.CharField(max_length=225)
    website = models.CharField(max_length=225, unique=True)
    attraction_info = models.CharField(max_length=550)
    open_hours = models.CharField(max_length=225, default="No info now")
    suggested_durations = models.FloatField(unique=False, default=0)
    latitude = models.FloatField(unique=False, null=False, default=0)
    longitude = models.FloatField(unique=False, null=False, default=0)
    image = models.ImageField(upload_to='attraction_image', blank=True)

    def __str__(self):
        if self.price == 0:
            return '%s - %s at %s for free' % (self.attraction_name, self.type, self.location.city)
        else:
            return '%s - %s at %s' % (self.attraction_name, self.type, self.location.city)

    def get_absolute_url(self):
        return reverse('Tripvisor_attraction_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['attraction_name', 'type', 'location']
        unique_together = (('attraction_name', 'location'),)


class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    user_account = models.CharField(max_length=25, unique=True)
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    nickname = models.CharField(max_length=25, blank=True, default='')

    def __str__(self):
        result = ''
        if self.nickname == '':
            result = '%s' % self.user_account
        else:
            result = '%s' % self.nickname
        return result

    def get_absolute_url(self):
        return reverse('Tripvisor_user_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['user_account']


class Tour(models.Model):
    tour_id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='users', on_delete=models.PROTECT)
    attraction = models.ForeignKey(Attraction, related_name='tours', on_delete=models.PROTECT)
    tour_name = models.CharField(max_length=45)
    date = models.DateField()
    duration = models.PositiveIntegerField(default=1)
    cost = models.IntegerField(unique=False)
    rate = models.PositiveIntegerField(default=3)
    feedback = models.CharField(max_length=550)

    def __str__(self):
        result = ''
        if self.rate >= 4:
            return 'Recommended! %s - %s by %s - %s star' % (self.tour_name, self.attraction.attraction_name,
                                                                self.user.__str__(), self.rate)
        else:
            return '%s - %s by %s - %s star' % (self.tour_name, self.attraction.attraction_name, self.user, self.rate)

    def get_absolute_url(self):
        return reverse('Tripvisor_tour_detail_urlpattern',
                       kwargs={'pk': self.pk}
                       )

    class Meta:
        ordering = ['attraction', 'user']


class OpenHours(models.Model):
    hours_id = models.AutoField(primary_key=True)
    attraction = models.ForeignKey(Attraction, related_name='attractions', on_delete=models.PROTECT)
    sun_open = models.FloatField(unique=False)
    mon_open = models.FloatField(unique=False)
    tue_open = models.FloatField(unique=False)
    wed_open = models.FloatField(unique=False)
    thur_open = models.FloatField(unique=False)
    fri_open = models.FloatField(unique=False)
    sat_open = models.FloatField(unique=False)
    sun_closed = models.FloatField(unique=False)
    mon_closed = models.FloatField(unique=False)
    tue_closed = models.FloatField(unique=False)
    wed_closed = models.FloatField(unique=False)
    thur_closed = models.FloatField(unique=False)
    fri_closed = models.FloatField(unique=False)
    sat_closed = models.FloatField(unique=False)


class AttractionN(StructuredNode):
    name = StringProperty(unique_index=True, required=True)


class UserN(StructuredNode):
    name = StringProperty(unique_index=True, required=True)
    attraction = RelationshipTo(AttractionN, 'Likes')