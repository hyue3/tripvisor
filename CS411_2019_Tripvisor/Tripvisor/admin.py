from django.contrib import admin
from .models import Location, Attraction, Tour, User

admin.site.register(Location)
admin.site.register(Attraction)
admin.site.register(Tour)
admin.site.register(User)
