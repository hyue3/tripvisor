from django.shortcuts import redirect, render
from django.db import connection

from Tripvisor.models import (
    Location,
    Attraction,
    User,
    Tour,
)


class ObjectCreateMixin:
    form_class = None
    template_name = ''

    def get(self, request):
        return render(
            request,
            self.template_name,
            {'form': self.form_class}
        )

    def post(self, request):
        bound_form = self.form_class(request.POST)
        if bound_form.is_valid():
            cursor = connection.cursor()
            cursor.execute('INSERT INTO Tripvisor_location(city, state, area) VALUES(%s,%s,%s)',
                           (request.POST.get('city'),request.POST.get('state'),request.POST.get('area')))
            return render(
                request,
                'Tripvisor/location_list.html',
                {'location_list': Location.objects.raw('select * from Tripvisor_location;')}
            )
        else:
            return render(
                request,
                self.template_name,
                {'form': bound_form}
            )
