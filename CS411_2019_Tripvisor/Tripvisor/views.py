from django.db import connection
from django.shortcuts import render, get_object_or_404, redirect
from django.views import View
from Tripvisor.utils import ObjectCreateMixin
import datetime
import calendar
from neomodel import db

from Tripvisor.forms import (
    LocationForm,
)

from Tripvisor.models import (
    Location,
    Attraction,
    User,
    Tour,
)


class LocationList(View):
    def get(self, request):

        return render(
            request,
            'Tripvisor/location_list.html',
            {'location_list': Location.objects.raw('SELECT * FROM Tripvisor_location;')})


class LocationDetail(View):
    def get(self, request, pk):
        location = get_object_or_404(
            Location,
            pk=pk
        )
        attraction_list = Attraction.objects.raw('SELECT * FROM Tripvisor_attraction WHERE location_id = %s;',
                                                 [location.location_id])
        return render(
            request,
            'Tripvisor/location_detail.html',
            {'location': location, 'attraction_list': attraction_list}
        )

    def post(self, request, pk):

        location = get_object_or_404(
            Location,
            pk=pk
        )
        attraction_list = Attraction.objects.raw('SELECT * FROM Tripvisor_attraction WHERE location_id = %s;',
                                                 [location.location_id])

        if(request.POST.get('StartDate') == "" or request.POST.get('EndDate') == ""):
            return render(
                request,
                'Tripvisor/location_detail.html',
                {'location': location, 'attraction_list': attraction_list}
            )


        start_date = datetime.datetime.strptime(request.POST.get('StartDate'), '%Y-%m-%d').date()
        end_date = datetime.datetime.strptime(request.POST.get('EndDate'), '%Y-%m-%d').date()

        delta = end_date - start_date  # as timedelta

        schedule = ""
        visited_list = "0"
        location_id = str(location.location_id)

        for i in range(delta.days + 1):
            day = start_date + datetime.timedelta(days=i)
            weekday = day.weekday()
            schedule = schedule + day.strftime("%Y-%b-%d") + ", Day " + str(i+1) + ", " + calendar.day_name[weekday] +"\n"

            if(weekday == 0):
                date_open = 'mon_open'
                date_close = 'mon_closed'
            elif(weekday == 1):
                date_open = 'tue_open'
                date_close = 'tue_closed'
            elif (weekday == 2):
                date_open = 'wed_open'
                date_close = 'wed_closed'
            elif (weekday == 3):
                date_open = 'thur_open'
                date_close = 'thur_closed'
            elif (weekday == 4):
                date_open = 'fri_open'
                date_close = 'fri_closed'
            elif (weekday == 5):
                date_open = 'sat_open'
                date_close = 'sat_closed'
            elif (weekday == 6):
                date_open = 'sun_open'
                date_close = 'sun_closed'

            first_query = "SELECT attraction_id, attraction_name, attraction_info, address, CASE WHEN " + date_open + " < 8 THEN 8 ELSE " + date_open + " END AS start_visit, suggested_durations, CASE WHEN " + date_open + " < 8 THEN 8 + suggested_durations ELSE " + date_open + " + suggested_durations END AS finish_visit FROM Tripvisor_attraction NATURAL JOIN Tripvisor_open_hours WHERE location_id = " + location_id + " AND attraction_id NOT IN (" + visited_list + ") ORDER BY finish_visit ASC LIMIT 1;"
            if (len(Attraction.objects.raw(first_query)) == 0):
                break
            attraction = Attraction.objects.raw(first_query)[0]
            new_id = str(attraction.attraction_id)
            new_name = attraction.attraction_name
            start_visit = str(attraction.start_visit)
            end_visit = str(attraction.finish_visit)
            description = attraction.attraction_info
            visited_list = visited_list + ', ' + new_id
            start_hour = str(int(attraction.start_visit))
            start_min = "00" if attraction.start_visit % 1.0 == 0 else "30"
            end_hour = str(int(attraction.finish_visit))
            end_min = "00" if attraction.finish_visit % 1.0 == 0 else "30"
            schedule = schedule + "Time: " + start_hour + ":" + start_min + " ~ " + end_hour + ":" + end_min + "\nAttraction: " + new_name + "\nDescription: " + description + "\n\n"
            for j in range(3):
                query = "SELECT attraction_id, attraction_name, attraction_info, address, " + end_visit + " AS last_att_end_time, between_hours, CASE WHEN " + date_open + " < " + end_visit + " + between_hours THEN " + end_visit + " + between_hours ELSE " + date_open + " END AS start_visit, suggested_durations, CASE WHEN " + date_open + " < " + end_visit + " + between_hours THEN " + end_visit + " + between_hours + suggested_durations ELSE " + date_open + " + between_hours + suggested_durations END AS finish_visit FROM (Tripvisor_attraction NATURAL JOIN Tripvisor_open_hours) NATURAL JOIN (SELECT attraction_id, attraction_name, CASE WHEN distance < 5 THEN 1 WHEN distance < 5 AND distance > 5 THEN 2 ELSE 3 END AS between_hours FROM Tripvisor_attraction a1 NATURAL JOIN (SELECT a2.attraction_id AS attraction_id, a2.attraction_name,  6371 * 2 * atan2(sqrt(sin(radians(a1.latitude-a2.latitude)/2)*sin(radians(a1.latitude-a2.latitude)/2)+ cos(radians(a1.latitude))*cos(radians(a2.latitude))*sin(radians(a1.longitude-a2.longitude)/2)*sin(radians(a1.longitude-a2.longitude)/2)),  sqrt(1 - (sin(radians(a1.latitude-a2.latitude)/2)*sin(radians(a1.latitude-a2.latitude)/2)+ cos(radians(a1.latitude))*cos(radians(a2.latitude))*sin(radians(a1.longitude-a2.longitude)/2)*sin(radians(a1.longitude-a2.longitude))/2))) AS distance FROM Tripvisor_attraction a1, Tripvisor_attraction a2  WHERE a1.location_id = " + location_id + " AND a2.location_id = " + location_id + " AND a1.attraction_id = " + new_id + " AND a1.attraction_id <> a2.attraction_id ORDER BY distance) AS distance_table) AS between_hour_table WHERE location_id = " + location_id + " AND attraction_id NOT IN (" + visited_list + ") AND " + date_close + " > " + end_visit + " + between_hours + suggested_durations ORDER BY finish_visit ASC LIMIT 1;"
                if (len(Attraction.objects.raw(query)) == 0):
                    break
                attraction = Attraction.objects.raw(query)[0]
                new_id = str(attraction.attraction_id)
                new_name = attraction.attraction_name
                start_visit = str(attraction.start_visit)
                end_visit = str(attraction.finish_visit)
                description = attraction.attraction_info
                visited_list = visited_list + ', ' + new_id
                start_hour = str(int(attraction.start_visit))
                start_min = "00" if attraction.start_visit % 1.0 == 0 else "30"
                end_hour = str(int(attraction.finish_visit))
                end_min = "00" if attraction.finish_visit % 1.0 == 0 else "30"
                schedule = schedule + "Time: " + start_hour + ":" + start_min + " ~ " + end_hour + ":" + end_min + "\nAttraction: " + new_name + "\nDescription: " + description + "\n\n"
            schedule = schedule + "\n"

        return render(
            request,
            'Tripvisor/location_detail.html',
            {'location': location, 'attraction_list': attraction_list, 'schedule': schedule}
        )


class LocationCreate(ObjectCreateMixin, View):
    form_class = LocationForm
    template_name = 'Tripvisor/location_form.html'


class LocationUpdate(View):
    form_class = LocationForm
    model = Location
    template_name = 'Tripvisor/location_form_update.html'

    def get_object(self, pk):
        return get_object_or_404(self.model, pk=pk)

    def get(self, request, pk):
        location = self.get_object(pk)
        context = {
            'form': self.form_class(instance=location),
            'location': location
        }

        return render(request, self.template_name, context)

    def post(self, request, pk):
        location = self.get_object(pk)
        bound_form = self.form_class(request.POST, instance=location)
        if bound_form.is_valid():
            cursor = connection.cursor()
            cursor.execute("UPDATE Tripvisor_location SET city = %s, state = %s, area = %s WHERE location_id = %s;",
                           [request.POST.get('city'), request.POST.get('state'), request.POST.get('area'),
                            location.location_id])
            attraction_list = Attraction.objects.raw('SELECT * FROM Tripvisor_attraction WHERE location_id = %s;',
                                                     [location.location_id])
            return render(
                request,
                'Tripvisor/location_detail.html',
                {'location': location, 'attraction_list': attraction_list}
            )
        else:
            context = {
                'form': bound_form,
                'location': location,
            }
            return render(request, self.template_name, context)


class LocationDelete(View):

    def get_object(self, pk):
        return get_object_or_404(Location, pk=pk)

    def get(self, request, pk):
        location = self.get_object(pk)
        tours = location.tours.all()
        if tours.count() > 0:
            return render(
                request,
                'Tripvisor/location_refuse_delete.html',
                {'location': location,
                 'tours': tours}
            )
        else:
            return render(
                request,
                'Tripvisor/location_confirm_delete.html',
                {'location': location}
            )

    def post(self, request, pk):
        location = self.get_object(pk)
        cursor = connection.cursor()
        cursor.execute("DELETE FROM Tripvisor_location WHERE location_id = %s;", [location.location_id])
        return redirect('Tripvisor_location_list_urlpattern')


class AttractionList(View):
    def get(self, request):
        return render(
            request,
            'Tripvisor/attraction_list.html',
            {'attraction_list': Attraction.objects.raw("SELECT * FROM Tripvisor_attraction;")}
        )


class AttractionDetail(View):
    def get(self, request, pk):
        attraction = get_object_or_404(
            Attraction,
            pk=pk
        )
        tour_list = Tour.objects.raw('SELECT * FROM Tripvisor_tour WHERE tour_id = %s;', [attraction.attraction_id])
        name = str(attraction.attraction_name)
        query = "MATCH (a1: AttractionN {name: '" + name + "'}) <-[l1: Likes]- (u: UserN) -[l2: Likes]-> (a2: AttractionN) Where a1.name <> a2.name Return Distinct a2.name, Count(a2) Order By Count(a2) Desc"
        results, meta = db.cypher_query(query)
        list_str = "('"
        for i in range(len(results)):
            list_str = list_str + results[i][0]
            if i < len(results) - 1:
                list_str = list_str + "','"
        list_str = list_str + "')"
        attraction_list = Attraction.objects.raw("SELECT * FROM Tripvisor_attraction WHERE attraction_name IN " + list_str)

        name = str(attraction.attraction_name)
        query = "MATCH (a1: AttractionN {name: '" + name + "'}) <-[l1: Likes]- (u: UserN) Return Distinct u.name"
        results2, meta2 = db.cypher_query(query)
        list_str2 = "('"
        for i in range(len(results2)):
            list_str2 = list_str2 + results2[i][0]
            if i < len(results2) - 1:
                list_str2 = list_str2 + "','"
        list_str2 = list_str2 + "')"
        user_list = User.objects.raw(
            "SELECT * FROM Tripvisor_user WHERE first_name IN " + list_str2)

        return render(
            request,
            'Tripvisor/attraction_detail.html',
            {'attraction': attraction, 'tour_list': tour_list, 'attraction_list': attraction_list, 'user_list': user_list}
        )


class UserList(View):
    def get(self, request):
        return render(
            request,
            'Tripvisor/user_list.html',
            {'user_list': User.objects.raw("SELECT * FROM Tripvisor_user;")}
        )


class UserDetail(View):
    def get(self, request, pk):
        user = get_object_or_404(
            User,
            pk=pk
        )
        tour_list = Tour.objects.raw('SELECT * FROM Tripvisor_tour WHERE tour_id = %s;', [user.user_id])
        name = str(user.first_name)
        query = "MATCH (a: AttractionN) <-[l: Likes]- (u: UserN {name: '"+name+"'}) Return Distinct a.name"
        results, meta = db.cypher_query(query)
        list_str = "('"
        for i in range(len(results)):
            list_str = list_str + results[i][0]
            if i < len(results) - 1:
                list_str = list_str + "','"
        list_str = list_str + "')"
        attraction_list = Attraction.objects.raw(
            "SELECT * FROM Tripvisor_attraction WHERE attraction_name IN " + list_str)
        return render(
            request,
            'Tripvisor/user_detail.html',
            {'user': user, 'tour_list': tour_list, 'attraction_list': attraction_list}
        )


class TourList(View):
    def get(self, request):
        return render(
            request,
            'Tripvisor/tour_list.html',
            {'tour_list': Tour.objects.raw("SELECT * FROM Tripvisor_tour;")}
        )


class TourDetail(View):
    def get(self, request, pk):
        tour = get_object_or_404(
            Tour,
            pk=pk
        )
        attraction = tour.attraction
        user = tour.user
        return render(
            request,
            'Tripvisor/tour_detail.html',
            {'tour': tour,
             'attraction': attraction,
             'user_list': user}
        )


def location_search(request):
    q = request.GET.get('q')
    location_list = Location.objects.filter(city__icontains=q) | Location.objects.filter(state__icontains=q)
    return render(request, 'Tripvisor/location_results.html', {'location_list': location_list})


def attraction_search(request):
    q = request.GET.get('q')
    # q = "'%"+q+"%'"
    attraction_list = Attraction.objects.filter(attraction_name__icontains=q)
    # attraction_list = Attraction.objects.raw("SELECT * FROM Tripvisor_attraction LIKE %s;", [q])
    return render(request, 'Tripvisor/attraction_results.html', {'attraction_list': attraction_list})
