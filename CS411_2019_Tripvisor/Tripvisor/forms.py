from django import forms

from Tripvisor.models import Location


class LocationForm(forms.ModelForm):
    class Meta:
        model = Location
        fields = '__all__'

    def clean_city(self):
        return self.cleaned_data['city'].strip()

    def clean_state(self):
        return self.cleaned_data['state'].strip()

    def clean_area(self):
        return self.cleaned_data['area'].strip()
