# Generated by Django 2.2.1 on 2019-10-31 09:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Attraction',
            fields=[
                ('attraction_id', models.AutoField(primary_key=True, serialize=False)),
                ('attraction_name', models.CharField(max_length=45, unique=True)),
                ('website', models.CharField(max_length=225, unique=True)),
                ('address', models.CharField(max_length=225)),
                ('price', models.IntegerField()),
                ('attraction_info', models.CharField(max_length=550)),
                ('type', models.CharField(max_length=45)),
            ],
            options={
                'ordering': ['attraction_name', 'type', 'location'],
            },
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('user_id', models.AutoField(primary_key=True, serialize=False)),
                ('user_account', models.CharField(max_length=25, unique=True)),
                ('first_name', models.CharField(max_length=45)),
                ('last_name', models.CharField(max_length=45)),
                ('nickname', models.CharField(blank=True, default='', max_length=25)),
            ],
            options={
                'ordering': ['user_account'],
            },
        ),
        migrations.CreateModel(
            name='Tour',
            fields=[
                ('tour_id', models.AutoField(primary_key=True, serialize=False)),
                ('date', models.DateField()),
                ('duration', models.PositiveIntegerField(default=1)),
                ('cost', models.IntegerField()),
                ('rate', models.PositiveIntegerField(default=3)),
                ('feedback', models.CharField(max_length=550)),
                ('attraction', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='tours', to='Tripvisor.Attraction')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='users', to='Tripvisor.User')),
            ],
            options={
                'ordering': ['attraction', 'user'],
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('location_id', models.AutoField(primary_key=True, serialize=False)),
                ('city', models.CharField(max_length=45)),
                ('state', models.CharField(max_length=45)),
                ('area', models.CharField(max_length=45)),
            ],
            options={
                'ordering': ['city', 'state', 'area'],
                'unique_together': {('city', 'state')},
            },
        ),
        migrations.AddField(
            model_name='attraction',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='tours', to='Tripvisor.Location'),
        ),
        migrations.AlterUniqueTogether(
            name='attraction',
            unique_together={('attraction_name', 'location')},
        ),
    ]
