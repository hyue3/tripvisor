from django.conf.urls import url
from django.urls import path

from Tripvisor import views
from Tripvisor.views import (
    LocationList,
    LocationDetail,
    LocationCreate,
    LocationUpdate,
    LocationDelete,
    AttractionList,
    AttractionDetail,
    UserList,
    UserDetail,
    TourList,
    TourDetail,)

urlpatterns = [
    url(r'^search_location/$', views.location_search, name='search'),

    url(r'^search_attraction/$', views.attraction_search, name='search'),

    path('location/',
         LocationList.as_view(),
         name='Tripvisor_location_list_urlpattern'),

    path('location/<int:pk>/',
         LocationDetail.as_view(),
         name='Tripvisor_location_detail_urlpattern'),

    path('location/create/',
         LocationCreate.as_view(),
         name='Tripvisor_location_create_urlpattern'),

    path('location/<int:pk>/update/',
         LocationUpdate.as_view(),
         name='Tripvisor_location_update_urlpattern'),

    path('location/<int:pk>/delete/',
         LocationDelete.as_view(),
         name='Tripvisor_location_delete_urlpattern'),

    path('attraction/',
         AttractionList.as_view(),
         name='Tripvisor_attraction_list_urlpattern'),

    path('attraction/<int:pk>/',
         AttractionDetail.as_view(),
         name='Tripvisor_attraction_detail_urlpattern'),

    path('user/',
         UserList.as_view(),
         name='Tripvisor_user_list_urlpattern'),

    path('user/<int:pk>/',
        UserDetail.as_view(),
         name='Tripvisor_user_detail_urlpattern'),

    path('tour/',
         TourList.as_view(),
         name='Tripvisor_tour_list_urlpattern'),

    path('tour/<int:pk>/',
         TourDetail.as_view(),
         name='Tripvisor_tour_detail_urlpattern'),
]
