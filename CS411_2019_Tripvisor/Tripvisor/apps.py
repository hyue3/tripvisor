from django.apps import AppConfig


class TripvisorConfig(AppConfig):
    name = 'Tripvisor'
